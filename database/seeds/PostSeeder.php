<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<15;$i++)
        {
            $post = factory('Schwag\Models\Post', 1)->create();
            $ghost = factory('Schwag\Models\Ghost', 1)->create();
            $ghost->posts()->save($post);
        }
    }
}
