<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Schwag\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Schwag\Models\Post::class, function (Faker\Generator $faker) {
   
    return [
        'name' => $faker->name ,
        'type' => 'Schwag Alert' ,
        'email' => $faker->email ,
        'title' => $faker->sentence ,
        'body' => $faker->paragraph ,
        'photo' => 'placeholder/schwag-alert.jpg' ,
        'reply' => $faker->sentence ,
        'approval' => rand(0,1) ,
        'accepted_terms' => false ,
    ];
});

$factory->define(Schwag\Models\Ghost::class, function (Faker\Generator $faker) {

    $data = '{"dma_code":"0","ip":"46.19.37.108","asn":"AS196752","latitude":52.3667,"country_code":"NL","offset":"2","country":"Netherlands","isp":"Tilaa B.V.","timezone":"Europe\/Amsterdam","area_code":"0","continent_code":"EU","longitude":4.9,"country_code3":"NLD"}';

    return [
            'query' => $faker->ipv4,
            'data' => $data
    ];
});
