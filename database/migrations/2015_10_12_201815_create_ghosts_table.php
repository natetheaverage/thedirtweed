<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGhostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ghosts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('query');
            $table->integer('user_id');
            $table->json('data');
            $table->timestamps();
        });
 
        Schema::create('ghost_post', function(Blueprint $table){
            $table->integer('ghost_id')->unsigned();
            $table->integer('post_id')->unsigned();

            $table->foreign('ghost_id')
                ->references('id')
                ->on('ghosts')
                ->onDelete('cascade');

            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');

            $table->primary(['ghost_id', 'post_id']);
        });

        Schema::create('ghost_user', function(Blueprint $table){
            $table->integer('ghost_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('ghost_id')
                ->references('id')
                ->on('ghosts')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->primary(['ghost_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ghost_post');
        Schema::drop('ghost_user');
        Schema::drop('ghosts');
    }
}
