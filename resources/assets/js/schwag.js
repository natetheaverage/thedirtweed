// Schwag App

Dropzone.autoDiscover = false;

require('vueify-insert-css');
require('vue-hot-reload-api');

var Vue = require('vue');
var app = require('./app');
Vue.use( require('vue-resource') );
Vue.use( require('vue-touch') );

module.exports = new Vue(app).$mount('#schwag');  

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');