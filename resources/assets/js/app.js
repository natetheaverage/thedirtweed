   import RollOverTextBtn from './components/RollOverTextBtn.vue';
   import Submission from './components/Submission.vue';
   import MainNav from './components/MainNav.vue';
   import Content from './components/Content.vue';
   import Single from './components/Single.vue';
   import Disqus from './components/Disqus.vue';
   import Login from './components/Login.vue';
   import Post from './components/Post.vue';

export default  {
    
    data() {
        return {
            userResource: this.$resource('/api/user'),
            likeResource: this.$resource('/api/like/:id'),
            currentView: $(vueData)[0].currentView,
            showButtonDetails: false,
            adBannerMode: false,
            submissionType: '',
            navName: '',
            navDetails: '',
            dataMode: false,
            editMode: false,
            isCool: false,
            currentPost: 0,
            likes: [],
            user: [], 
        }
    },
    
    methods: 
    {
        setGhostAndUser()
        {

            var that = this;
            $.getJSON("http://ip-api.com/json",
            function(json) { 
                that.userResource.save(
                    {ghost:json},
                    function(data){
                        that.user = data;
                        that.setLikes();
                    }
                    ).error(function (data, status, request){ 
                        console.log("Oops newPost = "+status) 
                    }
                )  
            });
        },
        
        login() 
        { this.currentView = 'login'; },

        goHome() 
        { this.currentView = 'content'; },

        editPost(id)
        {
            this.currentView = 'submission';
            this.currentPost = id;
        },
        
        newPost(name)
        {
            this.currentView = 'submission';
            this.submissionType = name;
        },

        hoverOn(name, details)
        {
            this.showButtonDetails = true;
            setTimeout(this.hoverOff, 3000)
            this.navName = name;
            this.navDetails = details;
        },

        hoverOff()
        {
            this.showButtonDetails = false;
            this.navName = '';
            this.navDetails = '';
        },

        setLikes() 
        {
            var that = this;
            this.likeResource.get({id: this.user.id},
                function(data){
                    that.likes = data;
                }
            )
        },

        initiateDisqus() 
        {
            window.disqus_shortname = 'thedirtweed'
            var s = document.createElement('script'); s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || 
                document.getElementsByTagName('BODY')[0]).appendChild(s);
        },

        toggleMenu() 
        {
            $(".nano").nanoScroller();
        }
    },

    components: {
        'rollovertextbtn': RollOverTextBtn,
        'submission': Submission,
        'mainnav': MainNav,
        'content': Content,
        'disqus': Disqus,
        'single': Single,
        'login': Login,
        'post': Post,
    },
    
        
    
    ready(){

        console.log('- app.js Loaded and ready -')
        this.setGhostAndUser();
    }
}

