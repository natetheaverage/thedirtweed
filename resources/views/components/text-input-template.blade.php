<script type="text/x-template" id="text-input-template">
    {{-- <!-- // <span v-show="isCool" class="input input--shoko" 
      v-class="input--filled : isFilled">
        <input 
            class="input__field input__field--shoko" 
            v-model="typeOf"
            type="text" 
            id="@{{title}}" />

        <label class="input__label input__label--shoko" 
        for="@{{title}}">
            <span class="input__label-content input__label-content--shoko">@{{title}}</span>
        </label>
        <svg class="graphic graphic--shoko" width="300%" height="100%" viewBox="0 0 1200 60" preserveAspectRatio="none">
            <path d="M0,56.5c0,0,298.666,0,399.333,0C448.336,56.5,513.994,46,597,46c77.327,0,135,10.5,200.999,10.5c95.996,0,402.001,0,402.001,0"/>
            <path d="M0,2.5c0,0,298.666,0,399.333,0C448.336,2.5,513.994,13,597,13c77.327,0,135-10.5,200.999-10.5c95.996,0,402.001,0,402.001,0"/>
        </svg>
    </span> --> --}}

    <span v-show="!isCool">
    <div class="mar-top">
    <label class="submit-text-label" 
        name="@{{title}}">
        @{{title}} 
    </label>
    <input  class="form-control submit-text-field" 
            v-model="typeOf"
            type="text" 
            name="@{{title}}"
            id="@{{title}}" />
    </div>
        
    </span>
</script>
