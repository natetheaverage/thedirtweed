<script type="text/x-template" id="photo-upload-template">
<form action="/api/images/upload" class="dropzone" id="photos">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input id="hook" name="hook" type="hidden" value="@{{post.id}}" />
    <div class="dz-preview dz-file-preview" >
        <div class="dz-details">
        <div class="dz-filename"><span data-dz-name></span></div>
        <div class="dz-size" data-dz-size></div>
        <img data-dz-thumbnail />
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
        <div class="dz-success-mark"><span>✔</span></div>
        <div class="dz-error-mark"><span>✘</span></div>
        <div class="dz-error-message"><span data-dz-errormessage></span></div>
    </div>
</form>
</script> 
