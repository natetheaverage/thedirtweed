<script type="text/x-template" id="mainnav-template">
    <nav id="schwag-menu">
        <div class="nano">
            <div class="nano-content">
                <div class="nav-title">
                    <span>Section</span>
                </div>
                <div class="nav-item">
                    <a class="btn btn-block btn-primary" v-link="">State</a>
                </div>
                <div class="nav-item">
                    <a class="btn btn-block btn-primary" v-link="">Type</a>
                </div>
                <div class="nav-item">
                    <a class="btn btn-block btn-primary" href="/auth/logout">Logout</a>
                </div>
            </div>
        </div>
    </nav>
</script>
