<script type="text/x-template" id="post-template">
    
    <div class="row">
    @can('be-average') 

        <div class="pull-right">
            <a v-link="{ name: 'edit', params: { id: id } }" 
            class="btn btn-success"> 
            <i class="fa" v-class="fa-thumbs-o-up: approval ,fa-thumbs-o-down: !approval"></i>   | <i class="fa fa-edit" ></i></a>
        </div>
        
    @endcan
        <div id="@{{ id }}" class="panel panel-default col-sm-12 pad-btm">
            <div class="panel-header col-sm-12">
                <h2>@{{title}}</h2>
            </div>
            

            <div class="col-sm-12">
                <img v-if="post.id > 0" class="img-responsive img-rounded" 
                    src='/crudimg/@{{ photo }}' />
            </div>
       
            <div class="col-sm-12 mar-top">
                <p><span class="post-body-intro">Schwag Alert: </span>@{{body}}</p>
            </div>
        
            <div class="col-sm-12">
                <h4 class="post-reply">@{{reply}}</h4>
            </div>
            <div class="btn-group btn-group-justified btn-lg col-sm-12">
                <a class="btn btn-lg btn-warning col-sm-4 like-btn"
                    v-on="click: like">
                    <i class="fa" 
                    v-class="fa-heart : isLiked,
                        fa-heart-o : !isLiked" 
                    ></i>
                </a>
                <a class="btn btn-lg btn-info col-sm-4 comments-btn"
                    href="http://thedirtweed.com/onblast/@{{id}}">
                   {{--  <!-- v-link="{name: 'onblast', params: {
                        id: id,
                        title: title
                    }}" --> --}}
                    <i class="fa fa-comments-o"></i> <span class="disqus-comment-count" data-disqus-identifier="#pot-talk-@{{id}}"></span>
                </a>
                <a class="btn btn-lg btn-primary col-sm-4 link-btn" 
                    v-on="click: shareLink">
                    <i class="fa fa-link" ></i>
                </a>
            </div>
            <disqus 
                v-if="openDisqus" 
                v-transition="expand"
            ></disqus>
        </div>
    </div>

    @can('be-average')
    <div class="row">
        <div class="col-sm-12">
            <pre v-show="dataMode">@{{ $data | json }}</pre>
        </div>
    </div>
    @endcan

</script>
