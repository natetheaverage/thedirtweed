<script type="text/x-template" id="submission-template">
    <div class="panel panel-primary pad-all">
        @can('be-average')
        <p class="pad-all col-sm-12">Editing Post: @{{post.id}}</p>
        @endcan
        <h4>Feel free to post anonymously!</h4>
        <p class="mar-btm">The email is so I may reach you for clairification. (Spelling etc..) </p>

        <textinputcomponent 
        title='Name' 
        type-of="@{{@ post.name}}"
        is-cool="@{{isCool}}">
        </textinputcomponent>

        <textinputcomponent 
        title='Email' 
        type-of="@{{@ post.email}}"
        is-cool="@{{isCool}}">
        </textinputcomponent>

        <textinputcomponent 
        title='* Subject / Title' 
        type-of="@{{@ post.title}}"
        is-cool="@{{isCool}}">
        </textinputcomponent>
        
        <div class="col-sm-12 mar-top mar-btm">
            <label class="submit-text-label" name="post-body">*Details</label>
            <textarea name="post-body" class="submit-post-body" v-model="post.body"></textarea>
        </div>
    @can('be-average')
        <div class="pad-all col-sm-12">
            <textinputcomponent
            title='Reply' 
            type-of="@{{@ post.reply}}"
            is-cool="@{{isCool}}">
            </textinputcomponent>
        </div>
    @endcan

        <div class="col-sm-5">
            <label class="submit-text-label" 
                name="post-photos">Photos</label>
            <photouploadcomponent
            post="@{{@ post}}"></photouploadcomponent>
        </div>

        <div class="control-group mar-top col-sm-7">
            <label for="select-state">State:</label>
            <select  id="select-state" placeholder="Pick a state...">
            </select>
            <label for="select-city" style="margin-top:20px">City: <small>optional</small></label>
            <select v-model="tag.state" id="select-city" placeholder="Pick a city..."></select>
        </div>

        <div class="control-group mar-top col-sm-7">
            <label for="input-tags">Tags:</label>
            <input type="text" 
                id="input-tags" 
                class="input-tags demo-default">
        </div>

@can('be-average')
        <div class="mar-top pad-all col-sm-12">
            <span> You approve this post?
                <input type="checkbox"
                            id="approval-checkbox"
                            data-off-title="Nope" 
                            data-on-title="Yup"
                            data-off-class="btn-warning" 
                            data-on-class="btn-primary" 
                            data-style="btn-group-sm" 
                            v-model="post.approval">
            </span>
        </div>
    @endcan
    
        <div class="mar-top pad-all col-sm-12">
            <span> * I aggree to the terms and conditions set fourth <a>here</a>: 
                <input type="checkbox"
                    id="terms-checkbox"
                    data-off-title="Disaggree" 
                    data-on-title="Aggree"
                    data-off-class="btn-warning" 
                    data-on-class="btn-primary" 
                    data-style="btn-group-sm" 
                    v-model="post.accepted_terms">
            </span>
        </div>
        <div class="mar-top col-sm-12">
            <div class="btn-group-vertical btn-block pull-right">
                <a class="btn btn-lg btn-primary" v-touch="tap:updatePost" v-on="click: updatePost">Blast It!</a>
                <a class="btn btn-lg btn-danger">Delete</a>
            </div>
        </div>
        <br class="clear" />
    </div>
    @can('be-average')
    <pre v-show="dataMode">@{{ $route.params.post | json }}</pre>
    <pre v-show="dataMode">@{{ $data | json }}</pre>
    @endcan

</script>
