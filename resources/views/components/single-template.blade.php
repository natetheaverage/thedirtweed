<script type="text/x-template" id="single-template">
    
    <pre v-show="dataMode">@{{ $route.params | json }}</pre>

            
            <post
                accepted_terms="@{{post.accepted_terms}}"
                open-disqus="@{{@ openDisqus }}"
                approval="@{{post.approval}}"
                title="@{{post.title}}"
                photo="@{{post.photo}}"
                reply="@{{post.reply}}"
                body="@{{post.body}}"
                id="@{{post.id}}">
            </post>
   
    @can('be-average')
    <pre v-show="dataMode">@{{ $data | json }}</pre>
    @endcan
</script>
 