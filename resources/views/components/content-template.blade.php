<script type="text/x-template" id="content-template">
    <div v-repeat="post in posts | orderBy id -1">
            <post
                accepted_terms="@{{post.accepted_terms}}"
                open-disqus="@{{@ openDisqus }}"
                approval="@{{post.approval}}"
                title="@{{post.title}}"
                photo="@{{post.photo}}"
                reply="@{{post.reply}}"
                body="@{{post.body}}"
                id="@{{post.id}}">
            </post>
    </div>

    @can('be-average')
    <pre v-show="dataMode">@{{ $data | json }}</pre>
    @endcan

</script>
 