<!DOCTYPE html>
<html lang="en">
<head>
@include('includes/header') 

<meta>
</head>
    <body>

        <div id="schwag" class="schwag-app">
        @include('includes/navbar')

        
        <!-- Schwag Component Templates -->
        @include('components/photo-upload-template')
        <!-- End Schwag Component Templates -->

            <div class="wrapper">
           
            <mainnav></mainnav>
                <div v-show="adBannerMode" id="ad-header" class="container">
                        <div class="fake-banner"></div>
                </div> {{-- end ad-header --}}
                <div id="main" class="container">
                    <div class="content">

                      @can('be-average')

                        <pre v-show="dataMode">@{{ $data | json }}</pre>

                      @endcan

                      <component 
                        :is="currentView"
                        :go-home="goHome"
                        :likes="likes"
                        :user="user"
                        :data-mode="dataMode"
                        :edit-mode="editMode"
                        :edit-post="editPost"
                        :is-cool="isCool"
                        :current-post="currentPost"
                        :submission-type="submissionType"
                      ></component>

                    </div> {{-- end content --}}
                </div> {{-- end container --}}
            </div> {{-- end wrapper --}}
            @include('includes.footer')
      </div> {{-- end schwag-app --}}

      <script type"text/javascript" src="{!! asset('js/vendor.js') !!}"></script>

      <script type"text/javascript" src="{!! asset('js/schwag.js') !!}"></script>
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-68954800-1', 'auto');
        ga('send', 'pageview');
      </script>
    </body>
</html>
