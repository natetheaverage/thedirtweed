<nav class="navbar navbar-inverse navbar-fixed-top">

  <div class="container">
    <div class="navbar-items">
      <div class="navbar-item navbar-item-left">
        {{-- <a class="link link--ilin mar-top" 
        v-on:mouseover="bangIt"
        ><span>s<small>chwag</small></span><span>a<small>lert</small></span></a> --}}
        <rollovertextbtn
          :clicked="newPost"
          :hover-on="hoverOn"
          :hover-off="hoverOff"
          details="You ran in to a horrible product and need to save others from purchasing this crap"
          name="Schwag Alert"
        ></rollovertextbtn>
      </div>
      <div class="navbar-item navbar-item-center pad-lft">
        <rollovertextbtn
          :clicked="newPost"
          :hover-on="hoverOn"
          :hover-off="hoverOff"
          details="Let the world know about people that make our budding industry a dark place. So that we all may avoid them."
          name="Jerk Alert"
        ></rollovertextbtn>
      </div>
      <div class="navbar-item navbar-item-right pad-lft">
        <rollovertextbtn
          :clicked="newPost"
          :hover-on="hoverOn"
          :hover-off="hoverOff"
          details="Have you attended an event that was thrown in the name of weed, how was it? What did you like? Dislike? What would change?"
          name="Bunk Times"
        ></rollovertextbtn>
      </div> 

      @can('be-average')
     {{-- <div class="navbar-item navbar-item-left">
        <a class="btn btn-dark btn-nav-toggle" v-on="click: toggleMenu" href="#schwag-menu"><i class="fa fa-navicon fa-rotate-90"></i></a>
      </div> --}}
      @endcan
      @can('be-average')
      <div class="navbar-item navbar-item-right navbar-search">
      
       {{-- <span class="input input--makiko">
            <input class="input__field input__field--makiko" type="text" id="input-16" />
            <label class="input__label input__label--makiko" for="input-16">
              <span class="input__label-content input__label-content--makiko"></span>
            </label>
          </span> --}}
          
      </div>
      @endcan
      {{-- <div class="navbar-item navbar-item-right">
          <a class="btn btn-primary btn-open-submission fa fa-camera" 
          > Blast Em!</a>
      </div> --}}
    @can('be-average')
      <div class="navbar-item navbar-item-right">
          <a v-on:click="dataMode = !dataMode" class="btn btn-primary"><i class="fa fa-folder-o"></i></a>
      </div>
      <div class="navbar-item navbar-item-right">
          <a v-on:click="editMode = !editMode" class="btn btn-primary"><i class="fa fa-edit"></i></a>
      </div>
      <div class="navbar-item navbar-item-right">
          <a v-on:click="isCool = !isCool" class="btn btn-primary"><i class="fa fa-paper-plane"></i></a>
      </div>
      @endcan
    </div>
    
  </div>

  <div class="nav-info" v-if="showButtonDetails">
    <p><span>@{{ navName }}</span>: @{{ navDetails }}</p>
  </div>

</nav>
