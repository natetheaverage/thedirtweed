<div class="col-md-12 footer bg-primary">
@can('be-average')
  <span>current view: @{{currentView}}</span>
@endcan
    <a href="/" class="btn btn-xs btn-primary pull-left">home</a>
    <a href="/terms" class="btn btn-xs btn-primary pull-left">terms</a>
    <a href="/copywrite" class="btn btn-xs btn-primary pull-left">copywrite</a>
    <a href="/privacy-policy" class="btn btn-xs btn-primary pull-left">privacy policy</a>
    <a href="/faq" class="btn btn-xs btn-primary pull-left">faq</a>

    <a  href="/auth/logout" 
        class="btn btn-xs btn-primary pull-right"
    ><i class="fa fa-xs fa-close"></i></a>
    <a v-on:click="login()" class="btn btn-xs btn-primary pull-right"><i class="fa fa-xs fa-pencil"></i> </a>
</div>
