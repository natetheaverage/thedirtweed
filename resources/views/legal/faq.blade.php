@extends('sub')

@section('content')
    
<div id="legal-faqs" class="post card">
<h1>Legal FAQs</h1>
<p>If you have a legal question that has not already been answered below, please feel free to ask it in the comments section at the bottom of the page. If you have a legal question that you do not want to ask in the comments section, please feel free to email <a href="mailto:legal@thedirty.com">legal@thedirty.com</a></p>
<hr>
<h2>DMCA</h2>
<div id="dmca" class="panel-group" role="tablist" aria-multiselectable="false">
<div class="panel panel-default faq-panels">
<div id="dmca-heading-1" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#dmca-1" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="dmca-1">If someone posted my photos without permission, will you take them down? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="dmca-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="dmca-heading-1">
<div class="panel-body">
<p>We will certainly take a look at the issue.  TheDirty.com generally complies with all valid DMCA removal requests (we will usually not comply with invalid requests). Please review our <a href="http://thedirty.com/copyright/" target="_blank">copyright page</a> for more info.</p>
</div>
</div>
</div>
</div>
<hr>
<h2>Law Enforcement</h2>
<div id="law-enforcement" class="panel-group" role="tablist" aria-multiselectable="false">
<div class="panel panel-default faq-panels">
<div id="law-enforcement-heading-1" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#law-enforcement-1" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="law-enforcement-1">I need to obtain user information from you. How should I handle that? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="law-enforcement-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="law-enforcement-heading-1">
<div class="panel-body">
<p>Please start by sending us an email from your official law enforcement email account to legal@thedirty.com. Just let us know what you’re asking for and why you need the information. Requests which include a police report are MUCH more likely to be viewed favorably. ALWAYS include a link to the specific post in question. Depending on the specific situation, we may require you to obtain a subpoena (a search warrant is not the proper investigative tool), but we will explain this further after you contact us.</p>
<p>Also, FYI — although we are happy to work with law enforcement dealing with legitimate investigations, please be aware that it is unlawful for any public official to use his/her governmental power to violate the First Amendment rights of an anonymous Internet speaker. In plain English, what this means is very simple — if you are a member of law enforcement and you offer to help a friend by issuing a subpoena without valid legal grounds for doing so, you are breaking the law and you could face significant personal liability pursuant to 42 U.S.C. § 1983.</p>
<p>To help demonstrate this point, take a look at this case: <a href="https://www.scribd.com/doc/258153981/Rich-v-City-of-Jacksonville-2010-WL-4403095-09-Cv-454-J-34MCR-M-D-fla-2010" target="_blank">Rich v. City of Jacksonville, 2010 WL 4403095 (M.D.Fla. 2010)</a>. As this case explains, if a public official obtains a subpoena seeking to unmask an anonymous Internet speaker without probable cause, the public official will not be entitled to qualified immunity and will face personal liability for violating the speaker’s constitutional rights.</p>
<p>Moral of the story to all good cops out there — please don’t even think about pursuing the identity of an anonymous Internet user unless you have probable cause to believe the person has committed a crime. Also, when contacting us please be prepared to explain what crime you are investigating and why probable cause exists to believe our user has committed that crime. We’re not trying to make your life harder; we are simply trying to protect the rights of our users unless and until there’s valid grounds to believe they have crossed the line.</p>
</div>
</div>
</div>
</div>
<hr>
<h2>Lawsuits &amp; 'Reputation Defenders'</h2>
<div id="lawsuits-and-reputation-defenders" class="panel-group" role="tablist" aria-multiselectable="false">
<div class="panel panel-default faq-panels">
<div id="lawsuits-and-reputation-defenders-heading-1" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#lawsuits-and-reputation-defenders-1" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="lawsuits-and-reputation-defenders-1">Should I Hire A Lawyer or SEO Service To Remove My Post? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="lawsuits-and-reputation-defenders-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="lawsuits-and-reputation-defenders-heading-1">
<div class="panel-body">
<p>We almost didn’t post an answer to this question. Why? Because it’s kind of like asking a cop who just arrested you whether you need to hire a lawyer. Would you really trust the cop’s advice at that point? But we are asked this question so frequently, we wanted to offer a few comments about it.</p>
<p>It’s no secret that there are lots of lawyers out there who are happy to take your money in exchange for helping you deal with an unflattering online post, either on TheDirty or another website. Some of these lawyers are good, and some are not. Same thing with SEO (search engine optimization) companies.</p>
<p>If you Google “how to remove a post from thedirty”, you will get TONS of ads and links for lawyers/law firms and SEO companies. Because we get so many removal requests, we are familiar with many of these folks (especially the lawyers), and we know that some of them are skilled professionals who can do a lot to help victims of online defamation. On the other hand, we also get tons of legal threats and demand letters from lawyers who clearly don’t have a clue about this area of law.</p>
<p>Unfortunately, because removing posts from TheDirty is generally not in our best interest, we really aren’t comfortable giving people advice on whether they should hire a lawyer, or whether they should spend money with an SEO company. In other words, it would be better for us if we only recommended BAD lawyers, so that’s why you really don’t want to ask us to recommend someone good.</p>
<p>What we can tell you is that we would NEVER advise anyone to hire a lawyer unless they have significant experience dealing with online defamation. This is a highly specialized area of law, and just because you have a lawyer friend who handles DUIs doesn’t mean he/she is qualified to help with online issues. If you’re thinking of hiring a lawyer, you should ask them how many cases of online defamation they’ve handled and what the outcome was.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="lawsuits-and-reputation-defenders-heading-2" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#lawsuits-and-reputation-defenders-2" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="lawsuits-and-reputation-defenders-2">Can I Pay You To Remove A Post? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="lawsuits-and-reputation-defenders-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="lawsuits-and-reputation-defenders-heading-2">
<div class="panel-body">
<p>No.  TheDirty doesn’t have a “pay to play” program, so please don’t ask if you can pay us to remove a post.</p>
<p>FYI — if you’re really interested, there is some legal authority that suggests a website owner CAN lawfully charge money to remove content.  In fact, a lot of people have accused websites like Yelp of doing exactly that — either removing negative reviews for money, or boosting the prominence of negative reviews unless you pay for “advertising”.  Is that extortion?  It might feel like it, but so far most courts have rejected that argument.</p>
<p>Consider this holding from a recent federal lawsuit against Yelp which accused it of altering reviews for money: “Plaintiffs’ allegations of extortion based on Yelp’s alleged manipulation of their review pages — by removing certain reviews and publishing others or changing their order of appearance — falls within the conduct immunized by § 230(c)(1).”  <a href="https://scholar.google.com/scholar_case?case=1614384457464751450&amp;hl=en&amp;as_sdt=6&amp;as_vis=1&amp;oi=scholarr" target="_blank">Levitt v. Yelp!, Inc., 2011 WL 5079526 (N.D.Cal. Oct. 26, 2011)</a>.</p>
<p>Regardless of whether TheDirty COULD legally accept payments to remove posts, that’s just not something we are willing to do at this time.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="lawsuits-and-reputation-defenders-heading-3" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#lawsuits-and-reputation-defenders-3" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="lawsuits-and-reputation-defenders-3">I live in Canada and we have different laws here. I am going to sue you… <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="lawsuits-and-reputation-defenders-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="lawsuits-and-reputation-defenders-heading-3">
<div class="panel-body">
<p>This is technical, but if you file a lawsuit in Canada, any judgment you obtain will be worthless here in the United States. For a more detailed discussion of this topic, check out this&nbsp;<a href="http://gingraslaw.com/can-foreign-plaintiffs-sue-u-s-based-websites-for-publishing-defamatory-content-not-really/" target="_blank">article here</a>.</p>
</div>
</div>
</div>
</div>
<hr>
<h2>Policies</h2>
<div id="policies" class="panel-group" role="tablist" aria-multiselectable="false">
<div class="panel panel-default faq-panels">
<div id="policies-heading-1" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#policies-1" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="policies-1">Does TheDirty ever block posts?  Are all submissions published? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="policies-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="policies-heading-1">
<div class="panel-body">
<p>Yes and no.</p>
<p>Back in 2007 when this site first started (as DirtyScottsdale.com), most of the content was hand-picked by our founder, Nik Richie. As the site evolved, more and more content was submitted by third parties, and a lot of that content was pretty offensive (porn, racism, violence, etc.) For a while, Nik would review all new submissions and would block the really nasty stuff while approving other material that he thought would be interesting and relevant to our users.</p>
<p>As of 2015, this has changed. Currently, Nik only reviews new submissions on the “main” or “front” page, meaning the stuff you see at http://thedirty.com/. Nik generally does NOT screen submissions for non-main pages such as http://thedirty.com/saskatoon/, although he will still occasionally review and add comments to such pages.  In most cases, you can tell if Nik has reviewed the post because he will add his own comment signed “-nik” in bold. If you see a post that doesn’t have a comment from Nik, that means he probably did not review the post before it was published.</p>
<p>Also, we do NOT publish all submissions. We can and will block and/or remove content that we deem to be offensive, uninteresting, or otherwise inappropriate for this site.</p>
<p>P.S.  We are sometimes contacted by people who ask us to block posts about them BEFORE they appear on the site.  For example, a person may email us and say: “Hey my ex-boyfriend is threatening to post embarrassing photos of me on TheDirty.  Can you please make sure not to publish anything he submits?”</p>
<p>Unfortunately, we cannot consider pre-publication requests like this. However, if you are the victim of revenge porn please review our policy discussed elsewhere on this page and let us know ASAP.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="policies-heading-2" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#policies-2" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="policies-2">I am under 18, I live in California and I posted something by mistake… <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="policies-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="policies-heading-2">
<div class="panel-body">
<p>The State of California recently passed a new “<a href="http://www.forbes.com/sites/ericgoldman/2013/09/24/californias-new-online-eraser-law-should-be-erased/" target="_blank">Online Eraser</a>” law which is supposed to require websites to delete content posted by minors when asked by the original author. The new law became effective on January 1, 2015 and you can read a copy <a href="http://leginfo.legislature.ca.gov/faces/billNavClient.xhtml?bill_id=201320140SB568" target="_blank">here</a>.</p>
<p>For various different reasons, we do not believe that California’s “Online Eraser” law applies to TheDirty.com. However, until the courts have resolved this issue we are willing to consider removal requests from California residents who posted content on our site while they were under the age of 18, subject to appropriate proof.</p>
<p>If you would like to request removal under this law, please send an email to legal@thedirty.com and put “California Online Eraser” in the subject line. Please include a link to the page in question and a clear copy of your government issued-ID. We may also request additional information as needed to process your request.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="policies-heading-3" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#policies-3" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="policies-3">Can I get the real name/IP address of the person who posted me? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="policies-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="policies-heading-3">
<div class="panel-body">
<p>To get information about the person who submitted a post or comment, at a minimum you’re going to need a subpoena and in some cases you might have to obtain a court order. The exact process to use is way beyond the scope of this FAQ page, but here’s a helpful article that explains some of the legal issues involved: <a href="https://www.citizen.org/documents/litigating-civil-subpoenas-to-identify-anonymous-internet-apeakers-paul-alan-levy.pdf" target="_blank">Litigating Civil Subpoenas to Identify Anonymous Internet Speakers</a>, by Paul Alan Levy.</p>
    <hr>
<div class="media">
<div class="media-left">
<img class="media-object" src="https://thedirty.com/wp-content/themes/the-dirty/images/avatar-gingras.png" alt="David Gingras" width="100">
</div>
<div class="media-body">
<h4 class="media-heading">David Gingras's Comment:</h4>
<p>Normally, we charge for giving legal advice, but here’s the basic idea — if you want to sue someone who posted unlawful content on TheDirty.com, you can certainly do that. Before going down that road, you should really stop and talk to a lawyer who has extensive experience in online defamation cases. For one thing, a lot of people don’t realize that the moment you file a lawsuit, copies of records/pleadings from the case can and probably will be permanently published online by the news media and by websites that index court proceedings. <br>
<br>
This means that if you file a lawsuit trying to remove something from the Internet, your plan could backfire….badly. The most famous example is known as the “Streisand Effect“. <br>
<br>
Anyway, let’s assume you have decided to sue the author despite the risks. All you need is the author’s IP address and any other contact information. That’s fine — but we will not release author information just because you ask for it. Our users have a constitutional right under the First Amendment to remain anonymous unless and until you PROVE (not just claim) that they have done something unlawful.</p>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="policies-heading-4" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#policies-4" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="policies-4">Do you have a policy regarding “revenge porn”? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="policies-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="policies-heading-4">
<div class="panel-body">
<p><strong>Yes – revenge porn is bad.</strong> It has no place on our site. However, figuring out what is and what’s not revenge porn can be extremely difficult or even impossible.</p>
<p>In most cases, by definition “revenge porn” means nude or sexually explicit photos that are posted without the consent of the person(s) in the photos. Unfortunately, people don’t submit photos with a notarized affidavit that says: “the person in this photo did (or did not) give consent for it to be posted.” This means when a photo is first submitted, we generally have no idea if the person shown wanted it to be published (some people like the attention) or whether it’s actually a private photo posted as part of an illegal attack.</p>
<p>Trust us — just because a photo contains partial or full nudity doesn’t mean it’s revenge porn. Lots of aspiring models consent to having their nude images posted on mainstream sites like modelmayhem.com or onemodelplace.com. This is how many amateur models find work, so the mere fact that someone is nude or partially nude in a photo doesn’t automatically mean it’s revenge porn.</p>
<p>Of course, some people also proudly post their own risque photos on social media like Instagram, Facebook, and Twitter. Some people also take sexy photos and (regrettably) share them with other people including boyfriends/girlfriends, and even strangers. Given the huge amount of content that people post online and the unlimited circumstances under which content can be shared, it can be extremely difficult to tell where a photo came from, whether the person in the photo consented to it being posted, and whether the person submitting it did so for purposes of revenge, or for some other legitimate reason.</p>
<p>Although it can be very hard to accurately determine what is and what’s not illegal, we can still try. As a responsible member of the Internet community, TheDirty.com views revenge porn as a serious problem. Even if we can’t stop this problem completely, we can and will do our best to work with victims of these attacks.</p>
<p>So here’s the deal — if someone posts a nude or sexually explicit photo of you without your consent, please let us know IMMEDIATELY. Send an email to: LEGAL@THEDIRTY.COM with the words “Revenge Porn Report” in the subject line. Your email MUST include a link to the post, NOT a link to search results.</p>
<p>EXAMPLES:<br>
YES -&gt; This is a direct link to a post: <a href="http://thedirty.com/gossip/sarah-jones-vs-dirty-world/">http://thedirty.com/gossip/sarah-jones-vs-dirty-world/</a><br>
NO -&gt; This is a link to search results: <a href="http://thedirty.com/?s=sarah+jones">http://thedirty.com/?s=sarah+jones</a></p>
<p>Also, while it is not mandatory, we strongly recommend sending a scan/photo of your driver’s license or other photo ID to verify that you are the person shown in the post. We promise that we will NEVER publish any material you submit in connection with a revenge porn complaint. This information will be kept confidential and will be used solely to confirm that you are the person shown in the post.</p>
<p>Again, for legal reasons we cannot promise that every revenge porn complaint will result in a post being removed. However, if we review the matter and agree that it appears the post might qualify as revenge porn, we will gladly take it down.</p>
    <hr>
<div class="media">
<div class="media-left">
<img class="media-object" src="https://thedirty.com/wp-content/themes/the-dirty/images/avatar-gingras.png" alt="David Gingras" width="100">
</div>
<div class="media-body">
<h4 class="media-heading">David Gingras's Comment:</h4>
<p>When emailing, you do NOT need to tell us your entire life story, and we don’t need a 20-page email explaining the history of how someone is stalking you, etc. All we need to know is that you are the person in the photo, and you didn’t consent for it to be posted anywhere online. Oh, and unless you have a police report, PLEASE don’t tell us about how your phone was stolen or your computer was hacked. We get it; no need to explain.  <br>
</p>
</div>
</div>
</div>
</div>
</div>
</div>
<hr>
<h2>Post Removal</h2>
<div id="post-removal" class="panel-group" role="tablist" aria-multiselectable="false">
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-1" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-1" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-1">Can I remove a post I submitted? (I’m the author) <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-1">
<div class="panel-body">
<p>So you submitted a post and now you want to take it down?  Easy, right?</p>
<p>Unfortunately, we usually cannot honor removal requests like this.  Why?  Because it’s often difficult and/or impossible for us to confirm whether the person submitting the request is actually the same person who submitted the post.</p>
<p>Also, a lot of these types of requests say something like this: “Hey, I submitted a post and it’s all true, but now I am being threatened with a lawsuit so I need the post removed.”  We do not want to reward these types of threats, so that’s another reason we generally will not remove a post in this situation. </p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-2" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-2" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-2">Will you remove my name from a post? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-2">
<div class="panel-body">
<p>For some reason, we have been getting a HUGE number of removal requests that say something like this: “Hey, can you please remove this post about me, and if not, will you at least remove my name?”</p>
<p>Look — removing a name is basically the same thing as removing the whole post. In the past, we were sometimes willing to help people by removing names (and we still do this as a courtesy in RARE cases), but it’s just not fair to do this for some people without do it for everyone.</p>
<p>We want to be fair to everyone, so we are no longer considering requests to remove names from posts.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-3" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-3" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-3">My lawyer says I can sue for $11 million if you don’t remove my post! <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-3">
<div class="panel-body">
<p>Here’s what the law says — website owners are NOT RESPONSIBLE for false or inaccurate information posted by users.  PERIOD.</p>
<p>This rule applies even if you email us and claim that something in a post is false.  We are not required to remove content upon “notice”, so please don’t send us threats like this: “Supposedly you and your site are protected by section 230 of the CDA and thats well and fine but if you do not remove the post at my request I do then have grounds to sue you on.”</p>
<p>Your lawyer says our position is wrong?  Ask your lawyer to read this: <a href="http://www.ca6.uscourts.gov/opinions.pdf/14a0125p-06.pdf" target="_blank">Jones v. Dirty World, LLC, 755 F.3d 398 (6th Cir. 2014)</a>. And this: <a href="https://www.scribd.com/doc/154373355/S-C-v-Dirty-World-LLC-2012-WL-3335284-W-D-Mo-2012" target="_blank">S.C. v. Dirty World, LLC, 2012 WL 3335284 (W.D.Mo. 2012)</a>.</p>
<p>Then ask your lawyer for a full refund.</p>
<p>Not interested in reading boring court rulings? No problem, we got you covered. Just watch this instead:</p>
<p><iframe width="500" height="281" src="https://www.youtube.com/embed/_z8kvC681Vw?feature=oembed" frameborder="0" allowfullscreen=""></iframe></p>
<p>NOTE — the news story shown above was published in June 2014, and the plaintiff did not ask the U.S. Supreme Court to hear the case.  This means the judgment in favor of TheDirty is final.</p>
<p>For more information about this case, see below:</p>
<p><iframe width="560" height="315" src="https://www.youtube.com/embed/_kvEgGLttd4" frameborder="0" allowfullscreen=""></iframe></p>
<p style=" margin: 12px auto 6px auto; font-family: Helvetica,Arial,Sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; display: block;">   <a title="View Jones v. Dirty World - OPINION on Scribd" href="https://www.scribd.com/doc/229927122/Jones-v-Dirty-World-OPINION" style="text-decoration: underline;">Jones v. Dirty World – OPINION</a> by <a title="View David S. Gingras's profile on Scribd" href="https://www.scribd.com/DavidGingras" style="text-decoration: underline;">David S. Gingras</a></p>
<p><iframe class="scribd_iframe_embed" src="https://www.scribd.com/embeds/229927122/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-vd8mLEzLUUbf1WsPLb4X&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7729220222793488" scrolling="no" id="doc_6575" width="100%" height="600" frameborder="0"></iframe></p>
    <hr>
<div class="media">
<div class="media-left">
<img class="media-object" src="https://thedirty.com/wp-content/themes/the-dirty/images/avatar-gingras.png" alt="David Gingras" width="100">
</div>
<div class="media-body">
<h4 class="media-heading">David Gingras's Comment:</h4>
<p>Look — in the United States, website owners are generally not liable for content posted by users. If someone posts false information on Twitter, your dispute is with the user, not Twitter.  Same thing with Instagram, Facebook, YouTube -- any type of website where 3rd party users can post content. <br>
<br>
If someone posts false information about you on TheDirty, you can always sue the author and if you win your case, TheDirty will gladly consider removing any factual statements which a court has found to be false. But please don’t blame us just because someone else misused our site. Don’t shoot the messenger!</p>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-4" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-4" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-4">How Can I Remove A Post? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-4">
<div class="panel-body">
<p>We get a LOT of removal requests. As of early 2015, this site contains about 150,000 unique posts and about 4.5 million comments. That’s a lot of content.</p>
<p>Of the thousands of requests we see, approximately 90-95% say the same thing — someone posted lies about me, will you take the post down?</p>
<p>We refer to these requests as “truth-based” or “fact-based” because at the end of the day, the removal request is usually based on some sort of alleged false information in the post.</p>
<p><strong>Here’s our policy for these types of requests:</strong> with only a handful of narrow exceptions (i.e. STDs), any form of truth-based or fact-based request will NOT be considered. The minute we see a removal request arguing that something said in a post is false, we immediately stop reading.</p>
<p>Why? It’s not because TheDirty.com doesn’t care about the truth. Honestly, if we had a crystal ball and could allow people to only post comments that were true, we’d gladly do so. The problem is it doesn’t work that way.</p>
<p>We don’t consider truth-based requests for one simple reason — because we have no way of knowing which side is telling the truth and which side is lying. We have no ability to make credibility determinations or resolve factual disputes, so we will not consider any removal request that requires us to do either.</p>
<p>So, how can you remove a post?  Simple — get a court order showing that a post is false. This means you must go to court and prove that your version of the facts is correct and show that the author was lying. If you do that, we will gladly review the court’s order and, subject to our final editorial discretion, remove any material that we deem inappropriate.</p>
<p>To be clear, although it is our policy to comply with any valid/lawful court orders, we can’t promise that we will always comply with every order or judgment we receive. Also, to avoid any confusion caused by cases such as <a href="http://scholar.google.com/scholar_case?case=14909212640108844662&amp;hl=en&amp;as_sdt=6&amp;as_vis=1&amp;oi=scholarr" target="_blank">Barnes v. Yahoo!, Inc., 570 F.3d 1096 (9th Cir. 2009)</a>, nothing on this page or in our Terms of Service should be construed as a promise to remove any content.</p>
    <hr>
<div class="media">
<div class="media-left">
<img class="media-object" src="https://thedirty.com/wp-content/themes/the-dirty/images/avatar-gingras.png" alt="David Gingras" width="100">
</div>
<div class="media-body">
<h4 class="media-heading">David Gingras's Comment:</h4>
<p>Think about this example — two random strangers approach you on the street and each one tells you a story. Both say the other one is lying about the story. How could you decide which person to believe? Now, imagine this exact same thing happens to you 10, 20, 50 times a day…every day, all year long. Maybe the first time this happened you would feel a moral obligation to conduct a full investigation into the facts. But what about the 500th request? What about request #41,297? See the problem?    <br>
<br>
TheDirty.com does not want our users to post false information. There are so many good TRUE stories out there, we have no interest in accepting posts that are false. However, we are not the truth police and we cannot and will not investigate posts to separate fact from fiction. That’s not our job.    <br>
</p>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-5" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-5" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-5">Can I call someone to discuss removing a post? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-5">
<div class="panel-body">
<p>No. For legal reasons, we do not accept phone calls relating to posts — PERIOD, no exceptions. </p>
<p>If you call us and start to ask about how to remove a post, we will immediately end the call. We don’t want to be rude, but we also don’t want any misunderstandings about what was said on the phone. All communications relating to posts must be done in writing, preferably via email sent to legal@thedirty.com.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-6" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-6" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-6">I am under 18 – you have to remove my post, right? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-6">
<div class="panel-body">
<p>Sorry — this is a common misconception. Except in cases involving child pornography, there are no special rules against posting photos of minors. Legally, you can post a photo of someone under the age of 18 without their consent and without permission from their parents. That’s true on Twitter, Instagram, YouTube or The Dirty.</p>
<p>Now just because the law says you CAN do something doesn’t mean it’s always OK. So here’s our policy — except for cases which are clearly newsworthy (like stories about a child being kidnapped), we don’t want children under the age of 13 posted on this site. So, if someone submits a post about a person who is under the age of 13, please let us know immediately. In most cases, we will gladly remove photos of young children — no questions asked.</p>
<p>For anyone between the ages of 13-17, we review each request on a case-by-case basis. So, if a post shows a 16 or 17 year old and the comments are rude and not newsworthy, let us know. We can’t guarantee removal, but in most cases we take a dim view of anyone harassing young kids.</p>
    <hr>
<div class="media">
<div class="media-left">
<img class="media-object" src="https://thedirty.com/wp-content/themes/the-dirty/images/avatar-gingras.png" alt="David Gingras" width="100">
</div>
<div class="media-body">
<h4 class="media-heading">David Gingras's Comment:</h4>
<p>Unfortunately, we receive a fair number of complaints claiming that someone has posted child porn, but when we take a look, we find the report is almost always fake (because the photo either doesn’t contain porn, or it doesn’t contain a child, or both). For more information on what qualifies as child porn, please feel free to read this article: <a href="http://gingraslaw.com/legal-primer-what-is-and-is-not-child-pornography/" target="_blank">LEGAL PRIMER – WHAT IS (AND IS NOT) CHILD PORNOGRAPHY?</a></p>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-7" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-7" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-7">If I Am Being Harassed, Will You Remove My Post? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-7">
<div class="panel-body">
<p>Although TheDirty absolutely does not condone any use of our site for unlawful purposes, just because someone has criticized you doesn’t mean that qualifies as harassment.  This topic is extremely complicated, but here are some points to consider.</p>
<p>First of all, the line between free speech and harassment is not as clear as you might think. Want proof? Just consider this 83-page paper on the subject: <a href="https://www.scribd.com/doc/249666719/Caplan-Aaron-H-Free-Speech-and-Civil-Harassment-Orders-September-4-2012-64-Hastings-Law-Journal-781-April-2013-Loyola-LA-Legal-Studies-Paper" target="_blank">Aaron H. Caplan, Free Speech and Civil Harassment Orders, 64 Hastings Law Journal 781 (April 2013)</a>.</p>
<p>Don’t have time to read all that? OK, here’s a recent ruling from a federal court in Maryland which held, in effect, that stalking/harassment charges could NOT be based on comments the defendant posted on Twitter. See <a href="https://www.eff.org/files/filenode/cassidy-order-121511.pdf" target="_blank">United States v. Cassidy, Case No. 11-cv-00091 (D.Md. Dec. 15, 2011)</a>. The court’s decision was based on a very simple idea — if you don’t like something posted on a website, then just ignore it. In other words, the court said the victim could not claim she was harassed because she “had the ability to protect her ‘own sensibilities simply by averting’ her eyes from the Defendant’s Blog and not looking at, or blocking his Tweets.” (quoting <a href="http://www.law.cornell.edu/supct/html/98-1682.ZO.html" target="_blank">United States v. Playboy Entm’t Grp., Inc., 529 U.S. 803, 865 (2000)</a>).</p>
<p>In plain English, there’s a big difference between someone talking TO you versus talking ABOUT you. If someone repeatedly attempts to contact you directly in a threatening or upsetting manner, that might cross the line into criminal harassment and you should immediately report this to your local police. On the other hand, if someone is simply posting comments about you online that you don’t like, that is usually not sufficient to qualify as harassment because you can simply ignore the comments by not visiting the site. We know it may sound crazy, but if someone says something online that bothers you, the easiest solution is — DON’T READ IT!</p>
<p>Look, we are not saying that it’s impossible for Internet posts to cross the line into criminal territory. All websites that allow third parties to post content (such as Facebook, YouTube, Twitter, Instagram) can be misused in various ways that might be criminal. The problem is that in most cases, we can’t make that call and we can’t take sides. Unlike law enforcement, our staff is not equipped to investigate or intervene in cases of alleged harassment. For that reason, we can’t remove content or block someone solely because a post might be viewed as harassment.</p>
    <hr>
<div class="media">
<div class="media-left">
<img class="media-object" src="https://thedirty.com/wp-content/themes/the-dirty/images/avatar-gingras.png" alt="David Gingras" width="100">
</div>
<div class="media-body">
<h4 class="media-heading">David Gingras's Comment:</h4>
<p>LOOK — if you are seriously being stalked/harassed online, please don’t think we don’t care. We do care. The best advice we can offer is to contact your local police, report the situation, and then ask them to get in touch with us. We can’t promise that we’ll remove anything, but we are always willing to work with law enforcement in appropriate situations (a single rude/mean post usually isn’t sufficient).</p>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-8" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-8" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-8">But, I am seriously going to kill myself… <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-8">
<div class="panel-body">
<p>As a matter of policy, ALL threats of suicide are reported to the police. If you email us and mention suicide, you should expect a knock on your door from the police.</p>
<p>However, we cannot and will not remove content just because a person makes threats. If you or someone you know is actually considering suicide, please seek immediate help from the National Suicide Prevention Lifeline at (800) 273-8255.</p>
<p>Also, can we be honest? Your life is NOT worth losing over some stupid crap that someone posted about you on the Internet. Yes, words can hurt and people can be extremely cruel to each other. We understand that. But life is an amazing gift and you only get one shot at this.  NEVER waste that gift just because some asshole was rude to you on the Internet.</p>
<p>Here’s some friendly advice — if you get posted on this site and feel like your life is over, STOP. It’s not that bad. No matter what people say, you CAN rise above the haters. No matter how much something hurts, what doesn’t kill you makes you stronger.  Never forget that. Like Taylor Swift says: haters gonna hate. So just shake it off.</p>
<p>Nik Richie is a perfect example of this. People have been attacking and insulting him for YEARS. They make fun of Nik for being short. They make fun of his skin. They call him every name in the book. They constantly email him threats.</p>
<p>One time a person even created a Twitter account in the name of Rex Jagger — the child that Nik and Shayne lost.  This sick individual (we know exactly who it was) used the account to Tweet images of dead babies to Nik and Shayne, laughing about the fact that Nik’s son died before he was born. Think about that. </p>
<p>One of Nik’s stalkers even created a fake Facebook page in Nik’s name: https://www.facebook.com/nikrichie</p>
<p>We could go on, but you get the point. No matter who you are — rich/poor, hot/ugly, famous/not famous — you’re going to have some haters. The bottom line is that there are some seriously horrible, sick, and fucked up people in this world. That’s sad, but you do not have to let them affect you. You can rise above them, no matter what the haters say. You have friends and family who love and care about you. Haters can never take that away — unless you let them. So don’t let them.</p>
<p>Finally — keep in mind that unlike other websites, Nik is often willing to remove content even when he’s not legally required to. This means even if you ask Nik to remove something and he says no, you can ask again in a few months and maybe he will say yes. The point is that stuff posted on this website is probably not going to follow you for the rest of your life. There’s a good chance that eventually it will disappear, so keep that in mind.</p>
</div>
</div>
</div>
<div class="panel panel-default faq-panels">
<div id="post-removal-heading-9" class="panel-heading" role="tab">
<h4 class="panel-title"><a href="#post-removal-9" data-toggle="collapse" data-parent="#legal" aria-expanded="false" aria-controls="post-removal-9">If a post says I have STDs, can I submit medical records showing that’s false? <span class="pull-right"><i class="fa fa-chevron-down"></i></span></a></h4>
</div>
<div id="post-removal-9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="post-removal-heading-9">
<div class="panel-body">
<p><strong>Yes.</strong> BUT NOTE — we only consider requests that include written proof such as a recent medical test. If you ask us to remove an STD allegation without providing any records, your request will be automatically denied.  We do not consider removal requests without proof, so please don’t even ask unless you submit records with your request.</p>
<p>To be clear – we understand that people might be extremely uncomfortable with sending medical records to a website like TheDirty. We totally get this concern.  What if we turn around and post your records?  That would be humiliating, right?</p>
<p>LOOK – if you send us medical records to refute a claim of STDs, we absolutely guarantee that we will NOT post your records anywhere nor will we share them with anyone, PERIOD. We will keep your records strictly confidential and private and will use them only for the purpose of determining whether to remove something from a post. We know it might be hard to trust us, but it’s okay. You can trust us.</p>
</div>
</div>
</div>
</div>
</div>

@endsection