@extends('sub')

@section('content')
    
<p><strong>What information do we collect?</strong></p>
<p>We collect information from you when you submit content to our site.</p>
<p>When using our site, you may be asked to enter your: name, e-mail address, mailing address, or phone number. You may visit our site anonymously.</p>
<p><strong>What do we use your information for?</strong></p>
<p>Any information we collect from you may be used in one of the following ways:</p>
<li>To improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)</li>
<li>To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</li>
<li>To process transactions that you have specifically requested.</li>
<p>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested (if any).</p>
<p><strong>How do we protect your information?</strong></p>
<p><strong>Do we use cookies?</strong></p>
<p>We do not use cookies.</p>
<p><strong>Do we disclose any information to outside parties?</strong></p>
<p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information other than for the express purpose of delivering the purchased product or service requested. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential.  We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety.</p>
<p><strong>Third Party Links</strong></p>
<p>Occasionally, at our discretion, we may include or offer third party products or services on our website.  These third party sites have separate and independent privacy policies.  We therefore have no responsibility or liability for the content and activities of these linked sites.  Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
<p><strong>California Online Privacy Protection Act Compliance</strong></p>
<p>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent except as permitted by this policy or by law.</p>
<p><strong>Children’s Online Privacy Protection Act Compliance</strong></p>
<p>We are in compliance with the requirements of COPPA (Children’s Online Privacy Protection Act).  We do not collect any information from anyone under 13 years of age.  Our website, products and services are all directed to people who are at least 13 years old or older.</p>
<p><strong>Online Privacy Policy Only</strong></p>
<p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>
<p><strong>Your Consent<br>
</strong></p>
<p>By using our site, you consent to this privacy policy.</p>
<p><strong>Changes to our Privacy Policy</strong></p>
<p>If we decide to change our privacy policy, we will post those changes on this page.</p>
<p><strong>Contacting Us</strong></p>
<p>If there are any questions regarding this privacy policy you may contact via email at <a href="mailto:@.com?subject=Privacy Policy">asdf@dfgff.com</a>.</p>
<p>This policy was last modified on October 20, 2014</p>

@endsection