<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes/header') 

</head>
    <body>
    <div class="container ">
    <div class="panel panel-default col-sm-12 pad-all">
        @yield('content')
        </div>
        </div>
<div id="schwag" class="schwag-app">
 @include('includes.footer')
</div>
    <script type"text/javascript" src="{!! asset('js/vendor.js') !!}"></script>
    <script type"text/javascript" src="{!! asset('js/schwag.js') !!}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-68954800-1', 'auto');
        ga('send', 'pageview');
      </script>
    </body>
</html>