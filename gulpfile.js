var elixir = require('laravel-elixir');
//var vueify = require('laravel-elixir-browserify');//.init("vueify");
//var gulp = require('gulp');
//var browserify = require('browserify');
//var vueify = require('vueify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss')
        .browserSync()
        .browserify('schwag.js')
        .scripts([
        //'node_modules/vue/dist/vue.js',
        //'node_modules/vue-touch/vue-touch.js',
        //'node_modules/vue-router/dist/vue-router.js',
        //'node_modules/vue-resource/dist/vue-resource.min.js',
        //'node_modules/vue-validator/dist/vue-validator.js',
        'node_modules/sweetalert/dist/sweetalert-dev.js',
        'resources/assets/vendor/js/jquery-2.1.4.min.js',
        'resources/assets/vendor/js/nanoscroller.min.js',
        'resources/assets/vendor/js/bootstrap.min.js',
        'resources/assets/vendor/js/selectize.js',
        'resources/assets/vendor/js/dropzone.js',
        //'resources/assets/vendor/js/checkbox.js',
        //'resources/assets/vendor/css/AnimatedCheckboxes/js/svgcheckbx.js',
        'resources/assets/vendor/js/mmenu.min.js',
        //'resources/assets/vendor/js/selectize/dist/js/standalone/selectize.min.js',
        // 'resources/assets/vendor/js/greensock-js/src/minified/TweenLite.min.js',
        // 'resources/assets/vendor/js/greensock-js/src/minified/easing/EasePack.min.js',
        // 'resources/assets/vendor/js/greensock-js/src/minified/plugins/AttrPlugin.min.js',
        // 'resources/assets/vendor/js/greensock-js/src/minified/plugins/CSSPlugin.min.js',
        // 'resources/assets/vendor/js/ElasticProgress-master/dist/elastic-progress.min.js'

    ], 'public/js/vendor.js', './')
    
        .styles([
        'public/css/app.css',
        'node_modules/sweetalert/dist/sweetalert.css',
        'resources/assets/vendor/css/font-awesome.min.css'
    ], 'public/css/final.css', './')

        .version('public/css/final.css')

        
});
