<?php
namespace Schwag\Models;

use Schwag\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Ghost extends Model
{
  
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ghosts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['query', 'data',];

    /**
     * Relationship to Post model.
     *
     * @return   [collection]
     */
    public function posts()
    {
        return $this->hasMany('Schwag\Models\Post');
    }

    /**
     * Relationship to User model.
     *
     * @return   [collection]
     */
    public function user()
    {
        return $this->belongsToMany('Schwag\Models\User');
    }

    /**
     * Filter and Save JSON for Ghost model.
     *
     * @return   [Ghost Model]
     */
    public function persistanceAndFilter($ghostRequest) 
    {
        $ghostFilter['query'] = $ghostRequest['query'] ;

        $ghostFilter['data'] = serialize($ghostRequest) ;
        
        $user = User::create();
        $user->save(['name'=>'','email'=>'']);
        \Auth::login($user);

        $ghost = $this->firstOrCreate($ghostFilter);

        $user->ghosts()->save($ghost);

        

        return $ghost;
    }

    public function loadGhost(Request $request)
    {
        if( ! $ghost = Ghost::where('query', $request['ghost']['query'] )->first() )
        {
             $ghost = $this->persistanceAndFilter($request['ghost']);    
        }
        return $ghost;
    }
        

}
