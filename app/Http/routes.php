<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/home', function () {
    \JavaScript::put([
            'currentView' => 'content',
        ]);
    return view('theschwag');
});
Route::get('/', function () {
    \JavaScript::put([
            'currentView' => 'content',
        ]);
    return view('theschwag');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('/onblast/{id}', function($id)
{
    \JavaScript::put([
            'currentView' => 'single',
            'id' => $id,
        ]);
    return view('theschwag');
});


Route::get('/api/feed/{id}', function($id)
{
    if( \Gate::allows('be-average') ){
        return Schwag\Models\Post::where('id', '<', $id)
            ->orderBy('id', 'desc')
            ->take(5)
            ->get(['id','title','body','reply', 'photo', 'approval', 'accepted_terms']);
    }  
    return Schwag\Models\Post::where('approval', 1)
        ->where('id', '<=', $id)
        ->orderBy('id', 'desc')
        ->take(-5)
        ->get(['id','title','body','reply', 'photo']);//, 'accepted_terms'
});


Route::post('/api/images/upload', 'PostController@tempPhotos');
Route::resource('/api/like', 'LikeController');

Route::resource('/api/post', 'PostController');



Route::post('/api/user', function(Illuminate\Http\Request $requestedGhost)
{
    $ghost = new Schwag\Models\Ghost;
// dd($ghost->where('ip', $requestedGhost['ghost']['ip'] )->first());
    if( $ghost->where('query', $requestedGhost['ghost']['query'] )->first() !== null){

        $user = Schwag\User::whereId($ghost->loadGhost($requestedGhost)->user_id)
            ->with('ghosts')->first();
        
        return $user;
    }
    return ;
});

Route::post('/api/user/force', function(Illuminate\Http\Request $requestedGhost)
{
    $ghost = new Schwag\Models\Ghost;

    $user = Schwag\User::whereId($ghost->loadGhost($requestedGhost)->user_id)
        ->with('ghosts')->first();
    
    return $user;
    
});

Route::get('/api/tag', function(){
    $tags = Schwag\Models\Tag::all();
    return $tags;
});

Route::get('/faq', function () {
    return view('legal/faq');
});
Route::get('/terms', function () {
    return view('legal/terms');
});
Route::get('/copywrite', function () {
    return view('legal/copywrite');
});
Route::get('/privacy-policy', function () {
    return view('legal/privacy-policy');
});
