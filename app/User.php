<?php

namespace Schwag;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Relationship to Post model.
     * This is the "like/love" connection
     * @var array
     */
    public function likes()
    {
        return $this->belongsToMany('Schwag\Models\Post');
    }

    /**
     * Relationship to Tag model.
     *
     * @var array
     */
    public function tags()
    {
        return $this->belongsTo('Schwag\Models\Tag');
    }

    /**
     * Relationship to Ghost model.
     *
     * @var array
     */
    public function ghosts()
    {
        return $this->hasMany('Schwag\Models\Ghost');
    }

    /**
     * create array.
     *
     * @var array
     */
    public function getLikesArray($user)
    {
        $user->load(['likes' => function($query){$query->select('id');}]);
       $user = $user->likes->all();
       // dd(count($user) );
        if( count($user) > 0 ){
            foreach ($user as $key => $value) {
                $likesArray[$key] = $value->id;
            }
            return $likesArray;
        }
        return [];
    }


}

    